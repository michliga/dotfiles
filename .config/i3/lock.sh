#!/bin/bash

B='#00000066'  # black semitransparent
W='#ffffffaa'  # white semitransparent
D='#9a06f1ff'  # purple
A='#96266eff'  # alert

scrot /tmp/screenshot.png

corrupter /tmp/screenshot.png /tmp/screenshot_glitch.png

i3lock -i /tmp/screenshot_glitch.png \
--insidevercolor=$D   \
--ringvercolor=$W     \
--insidewrongcolor=$A \
--ringwrongcolor=$A   \
--insidecolor=$B      \
--ringcolor=$D        \
--linecolor=$B        \
--separatorcolor=$D   \
--verifcolor=$W       \
--wrongcolor=$A       \
--layoutcolor=$W      \
--keyhlcolor=$W       \
--bshlcolor=$W        \
--indicator           \
